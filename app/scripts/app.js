'use strict';

angular.module('myApp', ['ui.bootstrap', 'ui.router', 'restangular', 'home'])
    .config(function ($urlRouterProvider, $stateProvider, RestangularProvider) {

        RestangularProvider.setBaseUrl = 'http://139.162.17.230/wp-json/collegecourses/v1';
        $urlRouterProvider.otherwise('/home');
  });
