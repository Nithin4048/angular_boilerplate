'use strict';

angular.module('home', [])

.config(function($stateProvider) {

    $stateProvider
        .state('home', {
            url: '/home',
            //abstract: true,
            templateUrl: 'views/main.html',
            controller: 'MainCtrl',
        });
});
